import React, {useContext, useState} from 'react';
import {Button, Form} from "semantic-ui-react";
import gql from "graphql-tag";
import {useMutation} from "@apollo/client";
import {useForm} from "../utils/hooks";
import {AuthContext} from "../context/auth";

const Register = (props) => {
    const context = useContext(AuthContext);
    const [errors, setErrors] = useState({});

    const {values, onSubmit, onChange} = useForm(registerUser, {
        username: '',
        password: '',
        email: '',
        confirmPassword: ''
    });

    const [addUser, {loading}] = useMutation(REGISTER_USER, {
        update(_, {data: {register: userData}}) {
            context.login(userData);
            props.history.push('/');
        },
        onError(err) {
            setErrors(err.graphQLErrors[0].extensions.exception.errors);
        },
        variables: values
    });

    function registerUser() {
        addUser();
    }

    return (
        <div className={'form-container'}>
            <Form onSubmit={onSubmit} noValidate className={loading ? 'loading' : ''}>
                <h1>Register</h1>
                <Form.Input label={'username'} placeholder={'Username...'} name={'username'} value={values.username}
                            onChange={onChange} type={'text'} error={!!errors.username}/>
                <Form.Input label={'email'} placeholder={'email...'} name={'email'} value={values.email}
                            onChange={onChange} type={'text'} error={!!errors.email}/>
                <Form.Input label={'password'} placeholder={'password...'} name={'password'} value={values.password}
                            onChange={onChange} type={'password'} error={!!errors.password}/>
                <Form.Input label={'confirmPassword'} placeholder={'confirmPassword...'} name={'confirmPassword'}
                            value={values.confirmPassword} onChange={onChange} type={'password'}
                            error={!!errors.confirmPassword}/>
                <Button type={'submit'} primary>Register</Button>
            </Form>
            {Object.keys(errors).length > 0 && <div className={'ui error message'}>
                <ul className={'list'}> {Object.values(errors).map(error => <li key={error}>{error}</li>)}</ul>
            </div>}
        </div>
    );
};

const REGISTER_USER = gql`
    mutation register(
        $username: String!,
        $email:String!,
        $password:String!,
        $confirmPassword: String!
    ){
        register(
            registerInput:{
                username: $username,
                email: $email,
                password: $password,
                confirmPassword: $confirmPassword
            }){
            id, username, token, email, createdAt
        }

    }`;

export default Register;