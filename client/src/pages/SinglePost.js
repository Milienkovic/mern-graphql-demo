import React, {useContext, useRef, useState} from 'react';
import {gql, useMutation, useQuery} from "@apollo/client";
import {
    Button,
    CardContent,
    CardDescription,
    CardHeader,
    CardMeta,
    Form,
    Grid,
    Icon,
    Image,
    Label
} from "semantic-ui-react";
import {AuthContext} from "../context/auth";
import Card from "semantic-ui-react/dist/commonjs/views/Card";
import moment from "moment";
import LikeButton from "../components/LikeButton";
import DeleteButton from "../components/DeleteButton";
import MyPopup from "../utils/MyPopup";

const SinglePost = (props) => {
    const {user} = useContext(AuthContext);
    const [comment, setComment] = useState('');
    const commentInputRef = useRef(null)
    const postId = props.match.params.postId;
    const {data: {getPost} = {}} = useQuery(FETCH_POST_QUERY, {
        variables: {postId}
    });

    const [submitComment] = useMutation(SUBMIT_COMMENT_MUTATION, {
        update() {
            setComment('');
            commentInputRef.current.blur();
        },
        variables: {
            postId,
            body: comment
        }
    });

    function deletePostCallback() {
        props.history.push('/')
    }

    let postMarkup;

    if (!getPost) {
        postMarkup = <p>Loading post...</p>
    } else {
        const {id, body, username, createdAt, comments, likes, commentCount, likeCount} = getPost;

        postMarkup = (
            <Grid>
                <Grid.Row>
                    <Grid.Column width={2}>
                        <Image
                            src='https://react.semantic-ui.com/images/avatar/large/molly.png'
                            size={'small'}
                            float={'right'}
                        />
                    </Grid.Column>
                    <Grid.Column width={10}>
                        <Card fluid>
                            <Card.Content>
                                <Card.Header>{username}</Card.Header>
                                <Card.Meta>{moment(createdAt).fromNow(false)}</Card.Meta>
                                <Card.Description>{body}</Card.Description>
                            </Card.Content>
                            <hr/>
                            <Card.Content extra>
                                <LikeButton user={user} post={{id, likes, likeCount}}/>
                                <MyPopup content={'Comment on post'}>
                                    <Button as={'div'} labelPosition={'right'} onClick={() => {
                                        console.log('commenting post')
                                    }}>
                                        <Button basic color={'blue'}>
                                            <Icon name={'comments'}/>
                                        </Button>
                                        <Label basic color={'blue'} pointing={'left'}>{commentCount}</Label>
                                    </Button>
                                </MyPopup>
                                {user && user.username === username &&
                                <DeleteButton postId={id} callback={deletePostCallback}/>}
                            </Card.Content>
                        </Card>
                        {user && (
                            <Card fluid>
                                <CardContent>
                                    <p>Post a comment</p>
                                    <Form>
                                        <div className={'ui action input fluid'}>
                                            <input ref={commentInputRef} type={'text'} placeholder={'Comment...'}
                                                   name={'comment'}
                                                   value={comment} onChange={event => setComment(event.target.value)}/>
                                            <button type={'submit'} className={'ui button teal'}
                                                    disabled={comment.trim() === ''} onClick={submitComment}>Submit
                                            </button>
                                        </div>
                                    </Form>
                                </CardContent>
                            </Card>
                        )}
                        {comments.map(comment => (
                            <Card fluid key={comment.id}>
                                <CardContent>
                                    {user && user.username === comment.username && (
                                        <DeleteButton postId={id} commentId={comment.id}/>
                                    )}
                                    <CardHeader>{comment.username}</CardHeader>
                                    <CardMeta>{moment(createdAt).fromNow(false)}</CardMeta>
                                    <CardDescription>{comment.body}</CardDescription>
                                </CardContent>
                            </Card>
                        ))}
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        )
    }

    return (
        postMarkup
    );
};

const SUBMIT_COMMENT_MUTATION = gql`
    mutation ($postId: ID!, $body:String!){
        createComment(postId: $postId, body: $body){
            id
            comments{
                id
                body
                createdAt
                username
            }
            commentCount
        }
    }
`;

const FETCH_POST_QUERY = gql`
    query($postId: ID!){
        getPost(postId: $postId){
            id
            body
            createdAt
            username
            likeCount
            likes{
                username
            }
            commentCount
            comments{
                id
                username
                createdAt
                body
            }
        }
    }
`;

export default SinglePost;