import React, {useEffect, useState} from 'react';
import {Button, Card, Icon, Label} from "semantic-ui-react";
import {gql, useMutation} from "@apollo/client";
import {Link} from "react-router-dom";
import MyPopup from "../utils/MyPopup";

const LikeButton = ({post: {id, likes, likeCount}, user}) => {
    const [liked, setLiked] = useState(false);
    const [likePost] = useMutation(LIKE_POST_MUTATION, {
        variables: {postId: id},

    });

    useEffect(() => {
        if (user && likes.find(like => like.username === user.username)) {
            setLiked(true);
        } else {
            setLiked(false);
        }
    }, [user, likes]);

    const likeButton = user ? (
        liked ? (
            <Button color={'teal'}>
                <Icon name={'heart'}/>
            </Button>
        ) : (
            <Button color={'teal'} basic as={Link} to={'/login'}>
                <Icon name={'heart'}/>
            </Button>
        )
    ) : (
        <Button color={'teal'} basic as={Link} to={'/login'}>
            <Icon name={'heart'}/>
        </Button>
    );

    return (
        <Button as='div' labelPosition='right' basic onClick={user? likePost : undefined}>
            <MyPopup content={liked? 'Unlike' : 'Like'}>
                {likeButton}
            </MyPopup>
            <Label basic color='teal' pointing='left'>
                {likeCount}
            </Label>
        </Button>
    );
};

const LIKE_POST_MUTATION = gql`
    mutation  likePost($postId: ID!){
        likePost(postId: $postId){
            id, likes{
                id username
            }
            likeCount
        }
    }
`;

export default LikeButton;