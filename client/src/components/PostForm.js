import React from 'react';
import {Form, Button} from "semantic-ui-react";
import {useForm} from "../utils/hooks";
import {gql, useMutation} from "@apollo/client";
import {FETCH_POSTS_QUERY} from "../utils/graphql";

const PostForm = () => {
    const {values, onChange, onSubmit} = useForm(createPostCallback, {body: ''});
    const [createPost, {error}] = useMutation(CREATE_POST_MUTATION, {
        update(proxy, result) {
            const data = proxy.readQuery({query: FETCH_POSTS_QUERY});
            const posts = [result.data.createPost, ...data.getPosts];
            proxy.writeQuery({query: FETCH_POSTS_QUERY, data: {getPosts: posts}});
            values.body = '';
        }, onError(err) {
            return err;
        },
        variables: values
    });

    function createPostCallback() {
        createPost();
    }

    return (
        <>
            <Form onSubmit={onSubmit}>
                <h2>Create a post:</h2>
                <Form.Input
                    placeholder={'Hi world!'}
                    name={'body'}
                    onChange={onChange}
                    value={values.body}
                    error={!!error}
                />
                <Button type={'submit'} color={'teal'}>Submit</Button>
            </Form>
            {error && <div className={'ui error message'} style={{marginBottom: 20}}>
                <ul className={'list'}>
                    <li>{error.graphQLErrors[0].message}</li>
                </ul>
            </div>}
        </>
    );
};

const CREATE_POST_MUTATION = gql`
    mutation createPost($body: String!){
        createPost(body: $body){
            id, body, createdAt, username, likes{id, username,createdAt}, likeCount, commentCount, comments{id,body,createdAt,username}
        }
    }
`;

export default PostForm;