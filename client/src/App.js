import React from 'react';
import {ApolloClient, ApolloProvider, createHttpLink, InMemoryCache} from "@apollo/client";
import {BrowserRouter as Router, Route} from 'react-router-dom';

import 'semantic-ui-css/semantic.min.css';
import './App.css';
import Home from "./pages/Home";
import Login from "./pages/Login";
import Register from "./pages/Register";
import MenuBar from "./components/MenuBar";
import {Container} from "semantic-ui-react";
import {AuthProvider} from "./context/auth";
import AuthRoute from "./utils/AuthRoute";
import {setContext} from "@apollo/client/link/context";
import SinglePost from "./pages/SinglePost";

const httpLink = createHttpLink({
    uri: 'http://localhost:5000',
});

const authLink = setContext(() => {
    const token = localStorage.getItem('jwtToken');
    return {
        headers: {
            Authorization: token ? `Bearer ${token}` : ''
        }
    }
})

const client = new ApolloClient({link: authLink.concat(httpLink), cache: new InMemoryCache()})

function App() {
    return (
        <ApolloProvider client={client}>
            <AuthProvider>
                <Router>
                    <Container>
                        <MenuBar/>
                        <Route exact path={'/'} component={Home}/>
                        <AuthRoute exact path={'/login'} component={Login}/>
                        <AuthRoute exact path={'/register'} component={Register}/>
                        <Route exact path={'/posts/:postId'} component={SinglePost}/>
                    </Container>
                </Router>
            </AuthProvider>
        </ApolloProvider>
    );
}

export default App;
