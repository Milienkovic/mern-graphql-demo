const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const {UserInputError} = require('apollo-server');

const User = require('../../models/User');
const {validateRegisterInput, validateLoginInput} = require("../../utils/validators");
const {SECRET_KEY} = require("../../config");

function generateToken(res) {
    return jwt.sign({
        id: res.id,
        email: res.email,
        username: res.username
    }, SECRET_KEY, {expiresIn: '1h'});
}

module.exports = {
    Mutation: {
        async login(_, {username, password}, context, info) {
            const {errors, valid} = validateLoginInput(username, password);
            if (!valid) {
                throw new UserInputError('Input errors', {errors})
            }
            const user = await User.findOne({username});

            if (!user) {
                errors.general = 'User not found';
                throw new UserInputError('User not found', {errors});
            }

            const match = await bcrypt.compare(password, user.password);
            if (!match) {
                errors.general = 'Wrong credentials';
                throw  new UserInputError('Wrong credentials', {errors});
            }

            const token = generateToken(user);
            return {
                ...user._doc,
                id: user._id,
                token
            }
        },

        async register(parent, {registerInput: {username, email, password, confirmPassword}}, context, info) {
            const {valid, errors} = validateRegisterInput(username, email, password, confirmPassword);
            console.log(valid)
            if (!valid) {
                throw new UserInputError('Errors', {errors});
            }

            const user = await User.findOne({username});
            if (user) {
                throw new UserInputError('username is taken', {
                    errors: {
                        username: 'username is taken already'
                    }
                })
            }
            password = await bcrypt.hash(password, 12);

            const newUser = new User({email, password, username, createdAt: new Date().toISOString()});

            const res = await newUser.save();

            const token = generateToken(res)

            return {
                ...res._doc,
                id: res._id,
                token
            }
        }
    }
}