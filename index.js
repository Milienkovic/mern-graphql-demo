const {ApolloServer, PubSub} = require('apollo-server');
const mongoose = require('mongoose');

const {MONGODB} = require('./config');
const typeDefs = require('./graphql/typeDefs');
const resolvers = require('./graphql/resolvers/index');

const pubsub = new PubSub();

const PORT = process.env.port || 5000;

const server = new ApolloServer({
    typeDefs, resolvers,
    context: ({req}) => ({req, pubsub})
});

const t = new Object()
mongoose.connect(MONGODB, {useNewUrlParser: true}).then(() => {
    console.log('mongodb connected');
    return server.listen({port: PORT});
}).catch(err => {
    console.log(err);
});
